import React, { Component } from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import firebase from 'firebase';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import LoginForm from './src/components/LoginForm';

class App extends Component {
  componentWillMount() {
    firebase.initializeApp({
    apiKey: 'AIzaSyCyczSWCxwsaMioWKlnRFmsP9GEziqZ7TY',
    authDomain: 'myapp-b9d90.firebaseapp.com',
    databaseURL: 'https://myapp-b9d90.firebaseio.com',
    projectId: 'myapp-b9d90',
    storageBucket: 'myapp-b9d90.appspot.com',
    messagingSenderId: '238291198082'
  });
  }
  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
  return (
    <Provider store={store}>
    <View>
      <LoginForm />
    </View>
    </Provider>
  );
}
}
export default App;
